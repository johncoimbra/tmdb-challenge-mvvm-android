**Teste de Android (Java)**

**Objetivo**

Criação de um projeto para listar os filmes em cartaz no cinema utilizando a API The Movie Database (TMDb). Fica a critério como as informações dos filmes serão mostradas, sendo essencial o nome do filme e o seu poster, quaisquer outras informações do filme também serão bem vindas.

**API**

Documentação da rota “filmes em cartaz” https://developers.themoviedb.org/3/movies/get-now-playing

Documentação da rota “poster do filme” https://developers.themoviedb.org/3/getting-started/images

**Requisitos:**

**Criação do projeto no repositório Git:**

Esse deve ser um projeto android que deve possuir:

- Tela com uma lista paginada contendo os filmes providos pela API onde cada item devem conter a imagem da capa do filme e o título do mesmo.
- Tela que exibe os detalhes de um filme ao se clicar em um filme na tela de lista de filmes
- Deve ser possível “favoritar” e “desfavoritar” um filme na tela de detalhes de um filme, e filtrar através de um menu na toolbar da tela de lista de filmes para se exibir apenas os favoritos. Essa lógica deve ser local, não dependendo de novas requisições à API.
- Utilização de MVP ou MVVM na estruturação do projeto.


**Bônus**

Testes no projeto / Lint no projeto / Utilização de outras rotas da API
